<?php
/**********************************************
* 	FlippingBook Gallery Component.
*	� Mediaparts Interactive. All rights reserved.
* 	Released under Commercial License.
*	www.page-flip-tools.com
**********************************************/

defined('_JEXEC') or die('Restricted access');

if (!file_exists(JPATH_SITE . DS . 'components' . DS . 'com_flippingbook' . DS . 'flippingbook.php')) {
	echo '<div style="color: #CC0000; background-color: #E6C0C0; padding: 10px; font-weight: bold; border-bottom: 3px solid #DE7A7B; border-top: 3px solid #DE7A7B;">' . JText::_('MOD_FLIPPINGBOOK_CHECK_COMPONENT_INSTALLATION') . '</div>';
} else {
	echo $params->get('pretext');
	
	$book_id_for_module = $params->get('id');
	require(JPATH_SITE . DS . 'components' . DS . 'com_flippingbook' . DS . 'views' . DS . 'book' . DS . 'tmpl' . DS . 'default.php');
	
	echo $params->get('posttext');
}