<?php
/**
 * Article title slider
 *
 * @package Article title slider
 * @subpackage Article title slider
 * @version   1.0 April, 2012
 * @author    Gopi.R
 * @copyright Copyright (C) 2010 - 2012 www.gopiplus.com, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 */

// no direct access
defined('_JEXEC') or die;

class modArticleSlider
{
	public function loadScripts(&$params)
	{
		$doc = &JFactory::getDocument();
		$doc->addScript(JURI::Root(true).'/modules/mod_article_title_slider/js/jquery-1.3.2.min.js');
		$doc->addScript(JURI::Root(true).'/modules/mod_article_title_slider/js/jquery.cycle.all.min.js');
		$doc->addStyleSheet(JURI::Root(true).'/modules/mod_article_title_slider/css/mod_article_title_slider.css');
	}
	
	public function getArticleList($args)
	{
      $db = &JFactory::getDBO();

      $nullDate = $db->getNullDate();
      $date =& JFactory::getDate();
      $now = $date->toMySQL();

	  $ats_category_id = $args['ats_category_id'];
	  $ats_no_of_items = $args['ats_no_of_items'];
	  $ats_no_of_chars = $args['ats_no_of_chars'];
	  $ats_order_field = $args['ats_order_field'];
	  $ats_use_cache = $args['ats_use_cache'];
	  $ats_order_by = $args['ats_order_by'];
      
      if ($ats_no_of_chars == 0){
        $ats_no_of_chars=999;
      }
      
      $query  = "select cn.id, ca.id as catid, ca.alias as catalias, cn.alias as conalias, cn.sectionid, ";
			$query .= "CASE WHEN CHAR_LENGTH(cn.alias) THEN CONCAT_WS(':', cn.id, cn.alias) ELSE cn.id END as slug, ";
			$query .= "CASE WHEN CHAR_LENGTH(ca.alias) THEN CONCAT_WS(':', ca.id, ca.alias) ELSE ca.id END as catslug, ";
      $query .= "if (length(cn.title)>".$ats_no_of_chars.",concat(substring(cn.title,1,".$ats_no_of_chars."),'...'),cn.title) as title, ";
	  $query .= "cn.introtext as introtext, ";
      $query .= "cn.title as fulltitle ";
      $query .= "from #__content as cn , #__categories as ca ";
      $query .= "where cn.id <> '' ";
      if($ats_category_id != ""){
        $query .= " AND cn.catid in (".$ats_category_id.") ";
      }
      
      $query .= " and state = 1 and ca.id=cn.catid ";

      $query .= ' and ( publish_up = '.$db->Quote($nullDate).' or publish_up <= '.$db->Quote($now).' )';
      $query .= ' and ( publish_down = '.$db->Quote($nullDate).' or publish_down >= '.$db->Quote($now).' )';
      
      if ($ats_order_field == "random"){
      	$query .= " order by RAND() ";
      }else{
        $query .= " order by ".$ats_order_field." ".$ats_order_by;
      }
      
      if ($ats_no_of_items != 0) {
        $query .= " limit ".$ats_no_of_items;
      }
      //echo $query;
      $db->setQuery($query);
      $items = ($items = $db->loadObjectList())?$items:array();
      return $items;
    }	
	
	
	function ats_dp_clean($excerpt, $substr=0) 
	{
		$string = strip_tags(str_replace('[...]', '...', $excerpt));
		if ($substr>0) 
		{
			$string = substr($string, 0, $substr);
		}
		return $string;
	}
}
?>