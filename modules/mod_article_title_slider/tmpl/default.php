<?php
/**
 * Article title slider
 *
 * @package Article title slider
 * @subpackage Article title slider
 * @version   1.0 April, 2012
 * @author    Gopi.R
 * @copyright Copyright (C) 2010 - 2012 www.gopiplus.com, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 */

// no direct access
defined('_JEXEC') or die;

@$ats_speed = @$args['ats_speed'];
@$ats_timeout = @$args['ats_timeout'];
@$ats_css = @$args['ats_css'];
@$ats_direction = @$args['ats_direction'];
if(!is_numeric($ats_timeout))
{
	$ats_timeout = 5000;
}
if(!is_numeric($ats_speed))
{
	$ats_speed = 700;
}
	
if ( ! empty($items) ) 
{
	$ats_count = 0;
	echo '<div id="ARTICLE-TITLE-'.$ats_css.'">';
	foreach ( $items as $item ) 
	{
		@$ats_title =  mysql_real_escape_string($item->title);
		@$ats_link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catslug, $item->sectionid));
		echo '<p><a href="'.$ats_link.'">'.$ats_title.'</a></p>';
		$ats_count++;
	}
	echo '</div>';
}
?>
<!-- start Article title slider -->
<script type="text/javascript">
$(function() {
$('#ARTICLE-TITLE-<?php echo $ats_css; ?>').cycle({
	fx: '<?php echo $ats_direction; ?>',
	speed: <?php echo $ats_speed; ?>,
	timeout: <?php echo $ats_timeout; ?>
});
});
</script>
<!-- end Article title slider -->