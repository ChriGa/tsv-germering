<?php
/**
 * Article title slider
 *
 * @package Article title slider
 * @subpackage Article title slider
 * @version   1.0 April, 2012
 * @author    Gopi.R
 * @copyright Copyright (C) 2010 - 2012 www.gopiplus.com, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 *
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once(dirname(__FILE__).DS.'helper.php');

@$args['ats_direction'] = $params->get('ats_direction');
@$args['ats_speed'] = $params->get('ats_speed');
@$args['ats_timeout'] = $params->get('ats_timeout');
@$args['ats_css'] = $params->get('ats_css');
@$args['ats_category_id'] = $params->get('ats_category_id');
@$args['ats_no_of_items'] = $params->get('ats_no_of_items');
@$args['ats_no_of_chars'] = $params->get('ats_no_of_chars');
@$args['ats_order_field'] = $params->get('ats_order_field');
@$args['ats_order_by'] = $params->get('ats_order_by');
@$args['ats_use_cache'] = $params->get('ats_use_cache');
@$args['moduleclass_sfx'] = $params->get('moduleclass_sfx');

$cache = & JFactory::getCache();

if (@$args['ats_use_cache']) 
{
  $items = $cache->call(array('modArticleSlider','getArticleList'),$args);
}
else
{
  $items = modArticleSlider::getArticleList($args);
}

modArticleSlider::loadScripts($params);

// include the template for display
require(JModuleHelper::getLayoutPath('mod_article_title_slider'));

?>