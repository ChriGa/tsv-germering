<?php
/**********************************************
* 	FlippingBook Gallery Component.
*	� Mediaparts Interactive. All rights reserved.
* 	Released under Commercial License.
*	www.page-flip-tools.com
**********************************************/

defined('_JEXEC') or die('Restricted access');

if (!file_exists(JPATH_SITE . DS . 'components' . DS . 'com_flippingbook' . DS . 'flippingbook.php')) {
	echo '<div style="color: #CC0000; background-color: #E6C0C0; padding: 10px; font-weight: bold; border-bottom: 3px solid #DE7A7B; border-top: 3px solid #DE7A7B;">' . JText::_('MOD_FLIPPINGBOOK_CATEGORIES_LIST_CHECK_COMPONENT_INSTALLATION') . '</div>';
} else {
	echo $params->get('pretext');
	
	$show_module = true;
	$columns_in_list = $params->get('columns');
	$layout = $params->get('layout');

	//LOAD COMPONENT PARAMETERS
	if ( !defined( "FB_version" ) ) {
		$db	= JFactory::getDBO();
		$query = "SELECT name, value FROM #__flippingbook_config";
		$db->setQuery($query);
		$c_rows = $db->loadObjectList();
		foreach ( $c_rows as $row ) {
			eval ( "DEFINE('FB_" . $row->name . "', '" . $row->value . "');" );
		}
	}

	require(JPATH_SITE . DS . 'components' . DS . 'com_flippingbook' . DS . 'views' . DS . 'categories' . DS . 'tmpl' . DS . $layout . '.php');
	
	echo $params->get('posttext');
}