<?php
/**********************************************
* 	FlippingBook Gallery Component.
*	� Mediaparts Interactive. All rights reserved.
* 	Released under Commercial License.
*	www.page-flip-tools.com
**********************************************/

defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgSearchFlippingBook extends JPlugin {

	function onContentSearchAreas() {
		static $areas = array(
			'flippingbook' => 'FlippingBook'
			);
			return $areas;
	}

	function onContentSearch($text, $phrase='', $ordering='', $areas=null) {
		
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$groups = implode(',', $user->getAuthorisedViewLevels());

		if (is_array($areas)) {
			//Return blank array if FlippingBook checkbox is not checked
			if (!array_intersect($areas, array_keys($this->onContentSearchAreas()))) {
				return array();
			}
		}

		$search_in_pages_descriptions = $this->params->get('search_in_pages_descriptions', 1);
		$limit = $this->params->def('search_limit', 50);
		
		$text = trim($text);
		if ($text == '') {
			return array();
		}


		$section = JText::_( 'FlippingBook' );

		$wheres = array();
		switch ($phrase) {
			case 'exact':
				$text		= $db->Quote( '%'.$db->getEscaped( $text, true ).'%', false );
				$wheres2 	= array();
				$wheres2[] 	= 'a.description LIKE '.$text;
				$wheres2[] 	= 'a.title LIKE '.$text;
				$where 		= '(' . implode( ') OR (', $wheres2 ) . ')';
				break;

			case 'all':
			case 'any':
			default:
				$words 	= explode( ' ', $text );
				$wheres = array();
				foreach ($words as $word)
				{
					$word		= $db->Quote( '%'.$db->getEscaped( $word, true ).'%', false );
					$wheres2 	= array();
					$wheres2[] 	= 'a.description LIKE '.$word;
					$wheres2[] 	= 'a.title LIKE '.$word;
					$wheres[] 	= implode( ' OR ', $wheres2 );
				}
				$where 	= '(' . implode( ($phrase == 'all' ? ') AND (' : ') OR ('), $wheres ) . ')';
				break;
		}

		switch ( $ordering ) {
			case 'alpha':
			default:
				$order = 'a.title ASC';
				break;
			case 'newest':
				$order = 'a.created DESC';
				break;
			case 'oldest':
				$order = 'a.created ASC';
				break;
			case 'popular':
				$order = 'a.hits DESC';
				break;
			case 'category':
				$order = 'b.title ASC';
				break;
		}

		$query = 'SELECT a.id, a.title, a.description AS text, b.title as section, a.created AS created'
		. ' FROM #__flippingbook_books as a'
		. ' INNER JOIN #__flippingbook_categories AS b ON b.id = a.category_id'
		. ' WHERE ('. $where .')'
		. ' AND a.published = 1'
		. ' AND b.published = 1'
		. ' ORDER BY '. $order;
		$db->setQuery( $query, 0, $limit );
		$rows_books = $db->loadObjectList();
		$result = $rows_books;

		foreach($rows_books as $key => $row) {
			$rows_books[$key]->href = 'index.php?option=com_flippingbook&view=book&id=' . $row->id;
			$rows_books[$key]->created = $row->created;
			$rows_books[$key]->browsernav = "";
			$rows_books[$key]->section = $row->section;
		}

		if ( $search_in_pages_descriptions == 1 ) {
			$wheres = "";
			$wheres = array();
			$wheres2 = "";
			$wheres2 = array();
			switch ($phrase) {
				case 'exact':
					$wheres2 	= array();
					$wheres2[] 	= 'a.description LIKE '.$text;
					$where 		= '(' . implode( ') OR (', $wheres2 ) . ')';
					break;

				case 'all':
				case 'any':
				default:
					$words 	= explode( ' ', $text );
					$wheres = array();
					foreach ($words as $word)
					{
						$word		= $db->Quote( '%'.$db->getEscaped( $word, true ).'%', false );
						$wheres2 	= array();
						$wheres2[] 	= 'a.description LIKE '.$word;
						$wheres[] 	= implode( ' OR ', $wheres2 );
					}
					$where 	= '(' . implode( ($phrase == 'all' ? ') AND (' : ') OR ('), $wheres ) . ')';
					break;
			}

			switch ( $ordering ) {
				case 'alpha':
				case 'popular':
				case 'category':
				default:
					$order = 'a.ordering ASC';
					break;
				case 'newest':
					$order = 'a.created DESC';
					break;
				case 'oldest':
					$order = 'a.created ASC';
					break;
			}

			$query = 'SELECT a.id, a.description AS text, a.ordering, b.id, b.title as title, a.created AS created'
			. ' FROM #__flippingbook_pages as a'
			. ' LEFT JOIN #__flippingbook_books AS b ON b.id = a.book_id'
			. ' WHERE ('. $where .')'
			. ' AND a.published = 1'
			. ' AND b.published = 1'
			. ' ORDER BY '. $order;
			$db->setQuery( $query, 0, $limit );
			$rows_pages = $db->loadObjectList();
			foreach($rows_pages as $key => $row) {
				$rows_pages[$key]->href = 'index.php?option=com_flippingbook&view=book&id=' . $row->id . '&page=' . $row->ordering;
				$rows_pages[$key]->created = $row->created;
				$rows_pages[$key]->browsernav = "";
				$rows_pages[$key]->section = "";
			}
			$result = array_merge($rows_books, $rows_pages);
		}

		return $result;
	}
}