<?php
/**********************************************
* 	FlippingBook Gallery Component.
*	� Mediaparts Interactive. All rights reserved.
* 	Released under Commercial License.
*	www.page-flip-tools.com
**********************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

class plgContentFlippingBook_Plugin extends JPlugin {
	public function onContentPrepare($context, &$article, &$params, $page = 0) {
		if (strpos($article->text, 'flippingbook_book') === false) {
			return true;
		}
		$regex = "#{flippingbook_book (.*?)}#s";
		$matches = array();
		preg_match_all($regex, $article->text, $matches, PREG_SET_ORDER);

		foreach ($matches as $match) {
			if (!file_exists(JPATH_SITE . DS . 'components' . DS . 'com_flippingbook' . DS . 'flippingbook.php')) {
				echo '<div style="color: #CC0000; background-color: #E6C0C0; padding: 10px; font-weight: bold; border-bottom: 3px solid #DE7A7B; border-top: 3px solid #DE7A7B;">' . JText::_('Make sure that FlippingBook Gallery Component is installed!') . '</div>';
			} else {
				$book_id_for_module = $match[1];
				$call_from_plugin = true;
				require(JPATH_SITE . DS . 'components' . DS . 'com_flippingbook' . DS . 'views' . DS . 'book' . DS . 'tmpl' . DS . 'default.php');
				$article->text = preg_replace("|$match[0]|", $output_html, $article->text);
			}
		}
	}
}