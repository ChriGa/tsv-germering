<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$poss = array('bottom1','bottom2','bottom3','bottom4','bottom5','bottom6');
$n = 0;
if ($this->countModules('bottom1')) $n++;
if ($this->countModules('bottom2')) $n++;
if ($this->countModules('bottom3')) $n++;
if ($this->countModules('bottom4')) $n++;
if ($this->countModules('bottom5')) $n++;
if ($this->countModules('bottom6')) $n++;

if ($n > 0) {

?>
<div class="bottom">
	<div class="row-fluid">
		<?php foreach ($poss as $i => $pos): ?>
			<?php if ($this->countModules($pos)) : ?>
			<div class="module_bottom1 position_<?php echo $pos; ?>">
				<jdoc:include type="modules" name="<?php print($detectAgent == "desktop ") ? $pos : $pos . "-tablet"; ?>" style="custom" />
			</div>
			<?php endif ?>
		<?php endforeach ?>
	</div> 	
</div>  	
<?php } ?>		
		
