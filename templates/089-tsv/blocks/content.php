<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$span = 9;
if ($this->countModules('position-6')) $span = 9;
if (!$this->countModules('position-6')) $span = 12;

?>

<div class="innerwidth content">
	<div class="row-fluid">
	
		<?php if ($this->countModules('position-6') && $detectAgent == "phone ") : ?>
			<div class="mobileInfoNav span3 right">		
				<jdoc:include type="modules" name="position-6" style="custom" />
			</div>
		<?php endif; ?>

		<main id="content" role="main" class="span<?php echo $span . $body_class . $option. ' view-' . $view. ($itemid ? ' itemid-' . $itemid : ''); ?>">
		<!-- Begin Content -->
		<jdoc:include type="modules" name="position-3" style="xhtml" />
		<jdoc:include type="message" />
		<jdoc:include type="component" />
		<jdoc:include type="modules" name="position-2" style="none" />
		<!-- End Content -->
		</main>
		<?php if ($this->countModules('position-6') && $detectAgent == "desktop ") : ?>
			<div class="offset1 span2 right">	
				<jdoc:include type="modules" name="position-6" style="custom" />
			</div>
		<?php endif; ?>
		<?php if ($this->countModules('position-6') && $detectAgent == "tablet ") : ?>
			<div class="span3 right">	
				<jdoc:include type="modules" name="position-6" style="custom" />
			</div>
		<?php endif; ?>		
		
	</div>
</div>