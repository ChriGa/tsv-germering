<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;

if($detectAgent == "phone ") { $isPhone = true; }
?>
	
<header id="header" class="fullwidth">
	<div class="innerwidth ">
		<div class="logoContainer <?php print(!$isPhone) ? "flex" : ""; ?>">
			<div class="flexItem">
				<img class="logoChar" src="<?php print(!$isPhone) ? "/images/logo-char.png" : "/images/logo-char-mobile.png"; ?>" alt="TSV UG Maskottchen Bild">
			</div>
			<?php if(!$isPhone) : ?>        	
				<div class="flexItem">
					<h2 class="headerH2"><span>TSV</span> UNTERPFAFFENHOFEN-GERMERING e.V.</h2>
				</div>		
			<?php endif; ?>
			<div class="flexItem">
				<a class="brand no-barba" href="<?php echo $this->baseurl; ?>">
					<img src="/images/tsv-logo.png" alt="Logo des TSV UG ">
				</a>
			</div>          
		</div>
	</div>
</header>
<?php if($isPhone) : ?>        	
	<div class="innerwidth">
		<h2 class="phone headerH2"><span>TSV</span> UNTERPFAFFENHOFEN-GERMERING e.V.</h2>
	</div>		
<?php endif; ?>