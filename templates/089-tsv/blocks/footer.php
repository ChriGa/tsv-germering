<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer">
	<div id="copyright" class="fullwidth">
		<div class="copyWrapper innerwidth flex">
			<p><img class="logoFooter" src="/images/tsv-logo-footer.png" alt="TSV Germering Logo" /></p>
			<p class="copyName">&copy; <?php print date("Y");?> TSV Unterpfaffenhofen-Germering e.V.</p>
			<p><a class="imprLink" href="/impressum.html">Impressum</a> / <a class="imprLink" href="/datenschutz.html">Datenschutz</a></p>
		</div>
	</div>	
</footer>
		