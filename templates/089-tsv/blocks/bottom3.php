<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 

defined('_JEXEC') or die;

$poss = array('bottom13','bottom14','bottom15','bottom16','bottom17','bottom18');
$n = 0;
if ($this->countModules('bottom13')) $n++;
if ($this->countModules('bottom14')) $n++;
if ($this->countModules('bottom15')) $n++;
if ($this->countModules('bottom16')) $n++;
if ($this->countModules('bottom17')) $n++;
if ($this->countModules('bottom18')) $n++;

if ($n > 0) {
?>
	<div class="bottom3 fullwidth">
		<div class="bottom3-wrap innerwidth">
			<?php foreach ($poss as $i => $pos): ?>
				<?php if ($this->countModules($pos)) : ?>
				<div class="module_bottom3 position_<?php echo $pos; ?>">
					<jdoc:include type="modules" name="<?php echo $pos ?>" style="custom" />
				</div>
				<?php endif ?>
			<?php endforeach ?> 	
		</div> 
	</div>	
<?php } ?>		
		
