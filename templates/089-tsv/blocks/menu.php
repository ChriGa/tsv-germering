<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die; 

?>


<?php if ($this->countModules('menu') && !$detect->isMobile()) : ?>
  <nav class="navbar-wrapper innerwidth">
      <div class="navbar">
        <div class="navbar-inner">
            <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>                                
    				<div class="nav-collapse collapse "  role="navigation">
    					<jdoc:include type="modules" name="menu" style="custom" />
    				</div>
            <jdoc:include type="modules" name="suche" style="xhtml" />
        </div>
      </div>
  </nav>
<?php else : ?>
  <div class="navbar-mobile">
    <div class="mm-btn">
        <a href="#menu" class="btn-menu">
          <p class="btnDesc">Men&uuml;</p>
            <div id="toggle" class="button_container">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </div>
        </a>
    </div>
    <nav id="menu" class="">
      <jdoc:include type="modules" name="menu-mobile" style="custom" />
    </nav>

  <form action="/" method="post" class="form-inline mobile">
    <input type="image" alt="Suchen" class="button" src="/templates/089-tsv/images/searchButtonMobile.png" onclick="this.form.searchword.focus();">
    <label for="mod-search-searchword" class="element-invisible">Suchen</label> 
    <input name="searchword" id="mod-search-searchword" maxlength="200" class="inputbox search-query" type="search" size="20" placeholder="Suchen ...">   
    <input type="hidden" name="task" value="search">
    <input type="hidden" name="option" value="com_search">
    <input type="hidden" name="Itemid" value="1156">
  </form> 
</div>   
<?php endif; ?>

