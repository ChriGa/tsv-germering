<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$poss = array('top7','top8','top9','top10','top11','top12');
$n = 0;
if ($this->countModules('top7')) $n++;
if ($this->countModules('top8')) $n++;
if ($this->countModules('top9')) $n++;
if ($this->countModules('top10')) $n++;
if ($this->countModules('top11')) $n++;
if ($this->countModules('top12')) $n++;

if ($n > 0) {
$span = 12/$n;
?>
<div class="top2">
	<div class="row-fluid">
		<?php foreach ($poss as $i => $pos): ?>
			<?php if ($this->countModules($pos)) : ?>
			<div class="span<?php echo $span; ?> module_top position_<?php echo $pos; ?>">
				<jdoc:include type="modules" name="<?php echo $pos ?>" style="xhtml" />
			</div>
			<?php endif ?>
		<?php endforeach ?>
	</div> 
</div>	
<?php } ?>	
