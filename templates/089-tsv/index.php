<?php
/**
 * @author   	info@089webdesign.de
 */
 
defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//mobioe detect varibalen:

if (!$detect->isMobile()) {	$detectAgent = "desktop "; $clientMobile = false; }
elseif (!$detect->isMobile() || $detect->isTablet()) {	$detectAgent = "tablet "; $clientMobile = true; } 
else { $detectAgent = "phone "; $clientMobile = true; }

?>
<!DOCTYPE html>
<html lang="<?=$this->language?>" <?php //amp ?> >
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>

</head>

	<!-- Body -->
<body id="body" class="site <?php print $detectAgent; print($detect->isMobile()) ? "mobile " : " "; ?>">
			<?php					
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			
			?>
		<div class="fullwidth site_wrapper ">
			<div class="fullwidth Shadow">
			<?php 
				// including header
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');
			?>
			</div>
			<?php
				// including breadcrumb
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');
			?>
			<div class="fullwidth Shadow">														
			<?php					
				// including content
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');					
			?>
			</div>
			<div class="fullwidth Shadow">
			<?php
				if($detectAgent == "desktop " || $detectAgent == "tablet ") {
					// including bottom
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
				}
			?>
			</div>
			<div class="fullwidth Shadow">
			<?php
				// including bottom2
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');	
			?>
			</div>
			<div class="fullwidth Shadow">
			<?php
				// including bottom3
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom3.php');	
			?>
			</div>
			<?php
				// including footer
				include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');						
			?>								
		</div>			
	<jdoc:include type="modules" name="debug" style="none" />
<script type="text/javascript">
	jQuery(document).ready(function() {
		<?php if ($menu->getActive() == $menu->getDefault()) : // Aktuelles Carousel nur auf Startseite ?>
			jQuery(window).load(function() { 
			jQuery('.carousel-inner div:first, ol.carousel-indicators li:first').addClass('active');
				});
				jQuery('.carousel').carousel({
					  interval: 4000
					});
				<?php endif; ?>
		<?php // <---END  Aktuelles Carousel?>
		
		<?php //CG: cancel Klick für Sportangebote ?>
		jQuery('.item-878 a.dropdown-toggle').click(function(e){
			e.preventDefault();
			return false;
		});

		<?php if ($detect->isMobile()) : ?> //mobile lazyLoad etc

				jQuery.extend(jQuery.lazyLoadXT, { 
					  edgeY:  100,
					  srcAttr: 'data-src'
					});
			//mmenu
			var $menu = jQuery('#menu'); 
			var $btnMenu = jQuery('.btn-menu');
            jQuery("#menu").mmenu({
               "extensions": [
					"effect-menu-zoom",
					"effect-panels-zoom",
					"pagedim-black",
  					"pageshadow"
               ],
               "offCanvas": {
                  "position": "right"
               },
               "counters": true
            });
            $menu.find( ".mm-next" ).addClass("mm-fullsubopen");

            <?php // infomenue smartphones: ?>
            	jQuery('.infoDropdown-toggle').click('on', function(e){
            		e.preventDefault();
            		jQuery(this).next('ul').slideToggle();
            		jQuery(this).toggleClass('open');
            			(jQuery(this).hasClass('open')) ? jQuery(this).html('schliessen') : jQuery(this).html('&ouml;ffnen');
            	});
  
		<?php else : ?> //desktop LazyLoad
					jQuery.extend(jQuery.lazyLoadXT, {
						  edgeY:  50,
					  	srcAttr: 'data-src'
					});    

			jQuery(".item-878 .nav-child").each(function(i) { //CG: sportangebote stehts active state checken bei Unterpunkt
				if(jQuery(this).children('li').hasClass("active")) {
				    jQuery(this).parent().addClass('active');
				    jQuery(this).closest('.nav.menu').find('.item-878').addClass('active');
				}
			}); 	
		<?php endif;  ?>

	});
</script>
</body>
</html>

